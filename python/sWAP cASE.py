def swap_case(sentence):
    for i in range(len(sentence)):
        if sentence[i].islower():
            sentence[i]=sentence[i].upper();
        else:
            sentence[i]=sentence[i].lower()
        
    sentence=''.join(sentence)
    return sentence


sentence=input()
sentence=list(sentence);
sentence=swap_case(sentence);
print(sentence);
        