def printer(n):
    l=[]
    for i in range(1,n+1):
        l.append(i)
    l=list(map(str,l))
    l=''.join(l)
    print(l)
    
if __name__ == '__main__':
    n = int(input())
    printer(n)
