#Day 2: Operators
#Task
#Given the meal price (base cost of a meal), tip percent (the percentage of the meal price being added as tip), and tax percent (the percentage of the meal price being added as tax) for a meal, find and print the meal's total cost.

#Input Format

#There are lines of numeric input:
#The first line has a double, (the cost of the meal before tax and tip).
#The second line has an integer, (the percentage of being added as tip).
#The third line has an integer, (the percentage of being added as tax)
#Output Format

#Print The total meal cost is totalCost dollars., where is the rounded integer result of the entire bill ( with added tax and tip)
#Calculations:



#We round to the nearest dollar (integer) and then print our result:

import sys
def bill(meal_cost,tip_percent,tax_percent):
    tip=float(meal_cost)*(float(tip_percent)/100.0)
    tax=float(meal_cost)*(float(tax_percent)/100.0)
    l=tip+tax+float(meal_cost)
   
    print("The total meal cost is",round(l), "dollars.")

if __name__ == "__main__":
    meal_cost = float(input().strip())
    tip_percent = int(input().strip())
    tax_percent = int(input().strip())
    bill(meal_cost,int(tip_percent),int(tax_percent))
    
